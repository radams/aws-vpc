###############################################################################################################
# Connectivity NACLs - Set up Network Access Control Lists for the CONNECTIVITY zone.
###############################################################################################################

# Create NACL for connectivity subnets
resource "aws_network_acl" "thisnaclctv" {
  vpc_id = aws_vpc.thisvpc.id

  subnet_ids = concat(
    aws_subnet.thissubctv2a.*.id,
    aws_subnet.thissubctv2b.*.id,
    aws_subnet.thissubctv2c.*.id,
    aws_subnet.thissubctv2d.*.id,
  )

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  tags = merge(
    local.tags,
    {
      "Name" = "acl-${var.name}-${var.env}-ctv"
    },
  )
}
