###############################################################################################################
# Ingress NACLs - Set up Network Access Control Lists for the INGRESS zone.
###############################################################################################################

# Create NACL for ingress subnets
resource "aws_network_acl" "thisnaclin" {
  vpc_id = aws_vpc.thisvpc.id

  subnet_ids = concat(
    aws_subnet.thissubin2a.*.id,
    aws_subnet.thissubin2b.*.id,
    aws_subnet.thissubin2c.*.id,
    aws_subnet.thissubin2d.*.id,
  )

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  tags = merge(
    local.tags,
    {
      "Name" = "acl-${var.name}-${var.env}-in"
    },
  )
}
