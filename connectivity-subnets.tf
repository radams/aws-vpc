###############################################################################################################
# Connectivity Subnets - Set up Subnets for all availability zones for the CONNECTIVITY zone.
###############################################################################################################

# Create connectivity subnet - 2a
resource "aws_subnet" "thissubctv2a" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.ctvsub2a[count.index]
  availability_zone = data.aws_availability_zones.available.names[0]
  count             = length(var.ctvsub2a)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-ctv-2a-%02d",
        count.index + 1,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create connectivity subnet - 2b
resource "aws_subnet" "thissubctv2b" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.ctvsub2b[count.index]
  availability_zone = data.aws_availability_zones.available.names[1]
  count             = length(var.ctvsub2b)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-ctv-2b-%02d",
        count.index + 11,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create connectivity subnet - 2c
resource "aws_subnet" "thissubctv2c" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.ctvsub2c[count.index]
  availability_zone = data.aws_availability_zones.available.names[2]
  count             = length(var.ctvsub2c)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-ctv-2c-%02d",
        count.index + 21,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create connectivity subnet - 2d
resource "aws_subnet" "thissubctv2d" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.ctvsub2d[count.index]
  availability_zone = data.aws_availability_zones.available.names[3]
  count             = length(var.ctvsub2d)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-ctv-2d-%02d",
        count.index + 31,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}
