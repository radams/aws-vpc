###############################################################################################################
# Private Database Route Table - Set up Route Table and Associations for the PRIVATE DATABASE zone.
###############################################################################################################

# Create private database route tables
resource "aws_route_table" "thisprv_db_rtb" {
  count  = var.enable_prv_db_subnets == true ? length(data.aws_availability_zones.available.names) : 0
  vpc_id = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = format("rtb-${var.name}-${var.env}-prv-db-%s", element(data.aws_availability_zones.available.names, count.index))
      "db"   = "true"
    }
  )
}

# Create Private Database Route to Transit Gateway
resource "aws_route" "thisprv_db_routedef" {
  #count                 = var.enableprvdefroute == true ? length(data.aws_availability_zones.available.names) : 0
  count                  = var.enableprvdefroute == true && var.enable_prv_db_subnets == true && var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprv_db_rtb.*.id, count.index)
  destination_cidr_block = var.tgwprefix[0]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprv_db_rtb]
}

resource "aws_route" "thisprv_db_route1" {
  count                  = var.enable_prv_db_subnets == true && var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprv_db_rtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[0]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprv_db_rtb]
}

resource "aws_route" "thisprv_db_route2" {
  count                  = var.enable_prv_db_subnets == true && var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprv_db_rtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[1]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprv_db_rtb]
}

# Associate private database route tables with private database subnets
resource "aws_route_table_association" "thisprv_db_assoc2a" {
  count          = var.enable_prv_db_subnets == true ? length(var.dbsub2a) : 0
  subnet_id      = element(aws_subnet.thissubprv_db_2a.*.id, count.index)
  route_table_id = aws_route_table.thisprv_db_rtb[0].id
  depends_on     = [aws_route_table.thisprv_db_rtb]
}

resource "aws_route_table_association" "thisprv_db_assoc2b" {
  count          = var.enable_prv_db_subnets == true ? length(var.dbsub2b) : 0
  subnet_id      = element(aws_subnet.thissubprv_db_2b.*.id, count.index)
  route_table_id = aws_route_table.thisprv_db_rtb[1].id
  depends_on     = [aws_route_table.thisprv_db_rtb]
}

resource "aws_route_table_association" "thisprv_db_assoc2c" {
  count          = var.enable_prv_db_subnets == true ? length(var.dbsub2c) : 0
  subnet_id      = element(aws_subnet.thissubprv_db_2c.*.id, count.index)
  route_table_id = aws_route_table.thisprv_db_rtb[2].id
  depends_on     = [aws_route_table.thisprv_db_rtb]
}

resource "aws_route_table_association" "thisprv_db_assoc2d" {
  count          = var.enable_prv_db_subnets == true ? length(var.dbsub2d) : 0
  subnet_id      = element(aws_subnet.thissubprv_db_2d.*.id, count.index)
  route_table_id = aws_route_table.thisprv_db_rtb[3].id
  depends_on     = [aws_route_table.thisprv_db_rtb]
}

# Associate private database route tables with S3 VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thisprv_db_s3" {
  count           = var.enable_prv_db_subnets == true ? length(data.aws_availability_zones.available.names) : 0
  vpc_endpoint_id = aws_vpc_endpoint.thiss3ep.id
  route_table_id  = element(aws_route_table.thisprv_db_rtb.*.id, count.index)
  depends_on      = [aws_route_table.thisprv_db_rtb]
}

# Associate private database route tables with DynamoDB VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thisprv_db_dynamodb" {
  count           = var.enable_prv_db_subnets == true ? length(data.aws_availability_zones.available.names) : 0
  vpc_endpoint_id = aws_vpc_endpoint.thisdynamoep.id
  route_table_id  = element(aws_route_table.thisprv_db_rtb.*.id, count.index)
  depends_on      = [aws_route_table.thisprv_db_rtb]
}