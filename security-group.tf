###############################################################################################################
# VPC Security Group - Set up Security Group for VPC Endpoints and other networking purposes.
###############################################################################################################

# Create security group
resource "aws_security_group" "thissg" {
  vpc_id = aws_vpc.thisvpc.id
  name   = "sg_${var.name}-${var.env}"

  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = var.prvprefix
  }

  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = var.prvprefix
  }

  egress {
    protocol    = "tcp"
    from_port   = var.sgports[0]
    to_port     = var.sgports[0]
    cidr_blocks = [var.defprefix]
  }

  egress {
    protocol    = "tcp"
    from_port   = var.sgports[1]
    to_port     = var.sgports[1]
    cidr_blocks = [var.defprefix]
  }

  tags = merge(
    local.tags,
    {
      "Name" = "sg-${var.name}-${var.env}"
    }
  )
}
