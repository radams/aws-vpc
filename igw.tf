# Create Internet gateway
resource "aws_internet_gateway" "thisigw" {
  vpc_id = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "igw-${var.name}-${var.env}"
    },
  )
}

# Create the IGW route table
resource "aws_route_table" "thisigwrtb" {
  vpc_id = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "rtb-${var.name}-${var.env}-igw"
    },
  )
}

# Associate the IGW to the IGW route table
resource "aws_route_table_association" "thisigwassoc" {
  gateway_id     = aws_internet_gateway.thisigw.id
  route_table_id = aws_route_table.thisigwrtb.id
  depends_on     = [aws_route_table.thisigwrtb]
}

# Public Routes - Using Gateway Load Balancer
resource "aws_route" "igwroute2a_gwlb" {
  count                  = var.enableingress == true ? 1 : 0
  route_table_id         = aws_route_table.thisigwrtb.id
  destination_cidr_block = var.pubsub2a[0]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2a[0].id
  depends_on             = [aws_route_table.thisigwrtb]
}

resource "aws_route" "igwroute2b_gwlb" {
  count                  = var.enableingress == true ? 1 : 0
  route_table_id         = aws_route_table.thisigwrtb.id
  destination_cidr_block = var.pubsub2b[0]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2b[0].id
  depends_on             = [aws_route_table.thisigwrtb]
}

resource "aws_route" "igwroute2c_gwlb" {
  count                  = var.enableingress == true ? 1 : 0
  route_table_id         = aws_route_table.thisigwrtb.id
  destination_cidr_block = var.pubsub2c[0]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2c[0].id
  depends_on             = [aws_route_table.thisigwrtb]
}

resource "aws_route" "igwroute2d_gwlb" {
  count                  = var.enableingress == true ? 1 : 0
  route_table_id         = aws_route_table.thisigwrtb.id
  destination_cidr_block = var.pubsub2d[0]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2d[0].id
  depends_on             = [aws_route_table.thispubrtb]
}