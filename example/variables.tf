# VPC
variable "vpc_defprefix" {
  description = "Default gateway prefix"
  type        = string
  default     = "0.0.0.0/0"
}

variable "vpc_prvprefix" {
  description = "List of private prefixes for routing"
  type        = list(any)
  default     = ["10.0.0.0/8", "172.16.0.0/12"]
}

variable "vpc_sgports" {
  description = "TCP ports used for security groups"
  type        = list(any)
  default     = ["80", "443"]
}

variable "vpc_cidr" {
  description = "The CIDR of the VPC"
}

variable "vpc_prvsub2a" {
  description = "List of private subnets in AZ 2a"
  type        = list(any)
}

variable "vpc_prvsub2b" {
  description = "List of private subnets in AZ 2b"
  type        = list(any)
}

variable "vpc_prvsub2c" {
  description = "List of private subnets in AZ 2c"
  type        = list(any)
}

variable "vpc_prvsub2d" {
  description = "List of private subnets in AZ 2d"
  type        = list(any)
}

variable "vpc_pubsub2a" {
  description = "List of public subnets in AZ 2a"
  type        = list(any)
}

variable "vpc_pubsub2b" {
  description = "List of public subnets in AZ 2b"
  type        = list(any)
}

variable "vpc_pubsub2c" {
  description = "List of public subnets in AZ 2c"
  type        = list(any)
}

variable "vpc_pubsub2d" {
  description = "List of public subnets in AZ 2d"
  type        = list(any)
}

variable "vpc_ctvsub2a" {
  description = "List of private subnets in AZ 2a"
  type        = list(any)
}

variable "vpc_ctvsub2b" {
  description = "List of private subnets in AZ 2b"
  type        = list(any)
}

variable "vpc_ctvsub2c" {
  description = "List of private subnets in AZ 2c"
  type        = list(any)
}

variable "vpc_ctvsub2d" {
  description = "List of private subnets in AZ 2d"
  type        = list(any)
}

variable "vpc_kentcgwasn" {
  description = "The Kent ASN for Customer Gateway"
  type        = string
}

variable "vpc_kentcgwip" {
  description = "The Kent IP for Customer Gateway"
  type        = list(any)
}

variable "vpc_kenttun1cidr" {
  description = "The Kent Tunnel 1 CIDR"
  type        = string
}

variable "vpc_kenttun2cidr" {
  description = "The Kent Tunnel 2 CIDR"
  type        = string
}

variable "vpc_awsasn" {
  description = "AWS ASN for VGW"
  type        = string
}

variable "vpc_enablevpn" {
  description = "Indicate whether to enable the VPN"
  type        = string
}

variable "vpc_enableflowlog" {
  description = "Indicate whether to enable VPC flow log"
  type        = string
}

variable "vpc_eplist" {
  description = "The list of Endpoint Interfaces that will be created"
  type        = list(any)
}