########################################################
# VPC
########################################################

# create a REI VPC
module "vpc" {
  source             = "git::https://git.rei.com/scm/tfmods/aws-vpc?ref=12.28"
  name               = var.rei_domain
  region             = var.rei_region
  cidr               = var.vpc_cidr
  env                = var.environment
  prvsub2a           = var.vpc_prvsub2a
  prvsub2b           = var.vpc_prvsub2b
  prvsub2c           = var.vpc_prvsub2c
  prvsub2d           = var.vpc_prvsub2d
  pubsub2a           = var.vpc_pubsub2a
  pubsub2b           = var.vpc_pubsub2b
  pubsub2c           = var.vpc_pubsub2c
  pubsub2d           = var.vpc_pubsub2d
  ctvsub2a           = var.vpc_ctvsub2a
  ctvsub2b           = var.vpc_ctvsub2b
  ctvsub2c           = var.vpc_ctvsub2c
  ctvsub2d           = var.vpc_ctvsub2d
  kentcgwasn         = var.vpc_kentcgwasn
  kentcgwip          = var.vpc_kentcgwip
  kenttun1cidr       = var.vpc_kenttun1cidr
  kenttun2cidr       = var.vpc_kenttun2cidr
  awsasn             = var.vpc_awsasn
  enablevpn          = var.vpc_enablevpn
  enableflowlog      = var.vpc_enableflowlog
  eplist             = var.vpc_eplist
  enableinternet     = false
  enableingress      = false
  enableprvdefroute  = var.vpc_enableprvdefroute
  transit_gateway_id = data.terraform_remote_state.tgw.outputs.tgw_us_east_2_core_1_id
  tags               = local.common_tags

}

## VPC OUTPUTS ###
output "vpc_vpcid" {
  description = "VPC ID for this account."
  value       = module.vpc.vpcid
}

output "vpc_cidr" {
  description = "VPC CIDR for this account."
  value       = module.vpc.cidr
}

# Route Table outputs
output "vpc_prvrtbid" {
  description = "VPC Route Table ID for private subnets."
  value       = module.vpc.prvrtbid
}

output "vpc_pubrtbid" {
  description = "VPC Route Table ID for public subnets."
  value       = module.vpc.pubrtbid
}

output "vpc_ctvrtbid" {
  description = "VPC Route Table ID for connectivity subnets."
  value       = module.vpc.ctvrtbid
}

# Private Subnet outputs
output "vpc_subprv2a" {
  description = "Private Subnet IDs for 2a"
  value       = module.vpc.subprv2aid
}

output "vpc_subprv2b" {
  description = "Private Subnet IDs for 2b"
  value       = module.vpc.subprv2bid
}

output "vpc_subprv2c" {
  description = "Private Subnet IDs for 2c"
  value       = module.vpc.subprv2cid
}

output "vpc_subprv2d" {
  description = "Private Subnet IDs for 2d"
  value       = module.vpc.subprv2did
}

# Public Subnet outputs
output "vpc_subpub2a" {
  description = "Public Subnet IDs for 2a"
  value       = module.vpc.subpub2aid
}

output "vpc_subpub2b" {
  description = "Public Subnet IDs for 2b"
  value       = module.vpc.subpub2bid
}

output "vpc_subpub2c" {
  description = "Public Subnet IDs for 2c"
  value       = module.vpc.subpub2cid
}

output "vpc_subpub2d" {
  description = "Public Subnet IDs for 2d"
  value       = module.vpc.subpub2did
}

# Connectivity Subnet outputs
output "vpc_subctv2a" {
  description = "Connectivity Subnet IDs for 2a"
  value       = module.vpc.subctv2aid
}

output "vpc_subctv2b" {
  description = "Connectivity Subnet IDs for 2b"
  value       = module.vpc.subctv2bid
}

output "vpc_subctv2c" {
  description = "Connectivity Subnet IDs for 2c"
  value       = module.vpc.subctv2cid
}

output "vpc_subctv2d" {
  description = "Connectivity Subnet IDs for 2d"
  value       = module.vpc.subctv2did
}