###############################################################################################################
# Connectivity Route Table - Set up Route Table and Associations for the CONNECTIVITY zone.
###############################################################################################################

# Create connectivity route tables
resource "aws_route_table" "thisctvrtb" {
  vpc_id = aws_vpc.thisvpc.id
  count  = length(data.aws_availability_zones.available.names)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "rtb-${var.name}-${var.env}-ctv-%s",
        element(data.aws_availability_zones.available.names, count.index),
      )
    },
  )
}

# Associate connectivity route tables with connectivity subnets
resource "aws_route_table_association" "thisctvassoc2a" {
  subnet_id      = element(aws_subnet.thissubctv2a.*.id, count.index)
  route_table_id = aws_route_table.thisctvrtb[0].id
  count          = length(var.ctvsub2a)
  depends_on     = [aws_route_table.thisctvrtb]
}

resource "aws_route_table_association" "thisctvassoc2b" {
  subnet_id      = element(aws_subnet.thissubctv2b.*.id, count.index)
  route_table_id = aws_route_table.thisctvrtb[1].id
  count          = length(var.ctvsub2b)
  depends_on     = [aws_route_table.thisctvrtb]
}

resource "aws_route_table_association" "thisctvassoc2c" {
  subnet_id      = element(aws_subnet.thissubctv2c.*.id, count.index)
  route_table_id = aws_route_table.thisctvrtb[2].id
  count          = length(var.ctvsub2c)
  depends_on     = [aws_route_table.thisctvrtb]
}

resource "aws_route_table_association" "thisctvassoc2d" {
  subnet_id      = element(aws_subnet.thissubctv2d.*.id, count.index)
  route_table_id = aws_route_table.thisctvrtb[3].id
  count          = length(var.ctvsub2d)
  depends_on     = [aws_route_table.thisctvrtb]
}
