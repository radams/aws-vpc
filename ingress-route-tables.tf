###############################################################################################################
# Ingress Route Table - Set up Route Table and Associations for the INGRESS zone
###############################################################################################################

# Create ingress route tables
resource "aws_route_table" "thisinrtb" {
  vpc_id = aws_vpc.thisvpc.id
  count  = length(data.aws_availability_zones.available.names)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "rtb-${var.name}-${var.env}-in-%s",
        element(data.aws_availability_zones.available.names, count.index),
      )
    },
  )
}

# Create Public Route to Internet Gateway
resource "aws_route" "thisinroute" {
  count                  = length(data.aws_availability_zones.available.names)
  route_table_id         = element(aws_route_table.thisinrtb.*.id, count.index)
  destination_cidr_block = var.defprefix
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thisinrtb]
}

# Associate ingress route tables with ingress subnets
resource "aws_route_table_association" "thisinassoc2a" {
  subnet_id      = element(aws_subnet.thissubin2a.*.id, count.index)
  route_table_id = aws_route_table.thisinrtb[0].id
  count          = length(var.insub2a)
  depends_on     = [aws_route_table.thisinrtb]
}

resource "aws_route_table_association" "thisinassoc2b" {
  subnet_id      = element(aws_subnet.thissubin2b.*.id, count.index)
  route_table_id = aws_route_table.thisinrtb[1].id
  count          = length(var.insub2b)
  depends_on     = [aws_route_table.thisinrtb]
}

resource "aws_route_table_association" "thisinassoc2c" {
  subnet_id      = element(aws_subnet.thissubin2c.*.id, count.index)
  route_table_id = aws_route_table.thisinrtb[2].id
  count          = length(var.insub2c)
  depends_on     = [aws_route_table.thisinrtb]
}

resource "aws_route_table_association" "thisinassoc2d" {
  subnet_id      = element(aws_subnet.thissubin2d.*.id, count.index)
  route_table_id = aws_route_table.thisinrtb[3].id
  count          = length(var.insub2d)
  depends_on     = [aws_route_table.thisinrtb]
}
