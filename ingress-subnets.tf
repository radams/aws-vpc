###############################################################################################################
# ingress Subnets - Set up Subnets for all availability zones for the INGRESS zone
###############################################################################################################

# Create ingress subnet - 2a
resource "aws_subnet" "thissubin2a" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.insub2a[count.index]
  availability_zone = data.aws_availability_zones.available.names[0]
  count             = length(var.insub2a)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-in-2a-%02d",
        count.index + 1,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create ingress subnet - 2b
resource "aws_subnet" "thissubin2b" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.insub2b[count.index]
  availability_zone = data.aws_availability_zones.available.names[1]
  count             = length(var.insub2b)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-in-2b-%02d",
        count.index + 11,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create ingress subnet - 2c
resource "aws_subnet" "thissubin2c" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.insub2c[count.index]
  availability_zone = data.aws_availability_zones.available.names[2]
  count             = length(var.insub2c)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-in-2c-%02d",
        count.index + 21,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}

# Create ingress subnet - 2d
resource "aws_subnet" "thissubin2d" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.insub2d[count.index]
  availability_zone = data.aws_availability_zones.available.names[3]
  count             = length(var.insub2d)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "DO NOT USE-subnet-${var.name}-${var.env}-in-2d-%02d",
        count.index + 31,
      )
      "Primary" = count.index == 0 ? "true" : "false"
    },
  )
}
