###############################################################################################################
# VPC - Set up VPC, Internet Gateway, and logging
###############################################################################################################

# Create VPC
resource "aws_vpc" "thisvpc" {
  cidr_block           = var.cidr
  enable_dns_hostnames = "1"
  enable_dns_support   = "1"

  tags = merge(
    local.tags,
    {
      "Name" = "vpc-${var.name}-${var.env}"
    },
  )
}

# Create CloudWatch Log Group for VPC Flowlogs
resource "aws_cloudwatch_log_group" "thisloggroup" {
  name  = "loggroup-flow-logs-${var.name}-${var.env}"
  count = var.enableflowlog ? 1 : 0
}

# Set up VPC Flowlogs and point to the log group
resource "aws_flow_log" "thisflowlog" {
  iam_role_arn    = aws_iam_role.thisflowlogrole[count.index].arn
  log_destination = aws_cloudwatch_log_group.thisloggroup[count.index].arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.thisvpc.id
  count           = var.enableflowlog ? 1 : 0
}