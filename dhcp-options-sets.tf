resource "aws_vpc_dhcp_options" "newdopt" {
  domain_name         = "${var.region}.compute.internal rei.com reicorpnet.com"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = merge(
    local.tags,
    {
      "Name" = "dopt-${var.name}-${var.env}"
    },
  )
}

resource "aws_vpc_dhcp_options_association" "newdoptassoc" {
  dhcp_options_id = aws_vpc_dhcp_options.newdopt.id
  vpc_id          = aws_vpc.thisvpc.id
}
