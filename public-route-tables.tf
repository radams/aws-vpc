###############################################################################################################
# Public Route Table - Set up Route Table and Associations for the PUBLIC zone.
###############################################################################################################

# Create public route tables
resource "aws_route_table" "thispubrtb" {
  vpc_id = aws_vpc.thisvpc.id
  count  = length(data.aws_availability_zones.available.names)

  tags = merge(
    local.tags,
    {
      "Name" = format("rtb-${var.name}-${var.env}-pub-%s", element(data.aws_availability_zones.available.names, count.index))
    }
  )
}

# Create private routes to Transit Gateway
resource "aws_route" "pubrtb_thisprvroute_0" {
  count                  = var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thispubrtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[0] # "10.0.0.0/8"
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "pubrtb_thisprvroute_1" {
  count                  = var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thispubrtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[1] #"172.16.0.0/12
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thispubrtb]
}

# Create public route to Internet Gateway
resource "aws_route" "thispubroute" {
  count                  = var.enableinternet ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thispubrtb.*.id, count.index)
  destination_cidr_block = var.defprefix
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

## PUBLIC ROUTES (this would be the REI, AWS, Apigee, and Tenable IPs) ##

# Create public routes with Internet Gateway - depends on enable_ingress variable
resource "aws_route" "publicroutes2a" {
  count                  = var.enableingress == false ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[0].id
  destination_cidr_block = local.public_routes[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2b" {
  count                  = var.enableingress == false ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[1].id
  destination_cidr_block = local.public_routes[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2c" {
  count                  = var.enableingress == false ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[2].id
  destination_cidr_block = local.public_routes[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

# all regions have at least 3 AVs, this will prevent trying to make a route in the 4th AV
resource "aws_route" "publicroutes2d" {
  count                  = var.enableingress == false && length(var.pubsub2d) > 0 ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[3].id
  destination_cidr_block = local.public_routes[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}


# Public Routes - Using Gateway Load Balancer
resource "aws_route" "publicroutes2a_gwlb" {
  count                  = var.enableingress == true ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[0].id
  destination_cidr_block = local.public_routes[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2a[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2b_gwlb" {
  count                  = var.enableingress == true ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[1].id
  destination_cidr_block = local.public_routes[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2b[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2c_gwlb" {
  count                  = var.enableingress == true ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[2].id
  destination_cidr_block = local.public_routes[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2c[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

# all regions have at least 3 AVs, this will prevent trying to make a route in the 4th AV
resource "aws_route" "publicroutes2d_gwlb" {
  count                  = var.enableingress == true && length(var.pubsub2d) > 0 ? length(local.public_routes) : 0
  route_table_id         = aws_route_table.thispubrtb[3].id
  destination_cidr_block = local.public_routes[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2d[0].id
  depends_on             = [aws_route_table.thispubrtb]
}



## Associate public route tables with public subnets
resource "aws_route_table_association" "thispubassoc2a" {
  count          = length(var.pubsub2a)
  subnet_id      = element(aws_subnet.thissubpub2a.*.id, count.index)
  route_table_id = aws_route_table.thispubrtb[0].id
  depends_on     = [aws_route_table.thispubrtb]
}

resource "aws_route_table_association" "thispubassoc2b" {
  count          = length(var.pubsub2b)
  subnet_id      = element(aws_subnet.thissubpub2b.*.id, count.index)
  route_table_id = aws_route_table.thispubrtb[1].id
  depends_on     = [aws_route_table.thispubrtb]
}

resource "aws_route_table_association" "thispubassoc2c" {
  count          = length(var.pubsub2c)
  subnet_id      = element(aws_subnet.thissubpub2c.*.id, count.index)
  route_table_id = aws_route_table.thispubrtb[2].id
  depends_on     = [aws_route_table.thispubrtb]
}

resource "aws_route_table_association" "thispubassoc2d" {
  count          = length(var.pubsub2d)
  subnet_id      = element(aws_subnet.thissubpub2d.*.id, count.index)
  route_table_id = aws_route_table.thispubrtb[3].id
  depends_on     = [aws_route_table.thispubrtb]
}




# Associate public route tables with S3 VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thispubs3" {
  vpc_endpoint_id = aws_vpc_endpoint.thiss3ep.id
  route_table_id  = element(aws_route_table.thispubrtb.*.id, count.index)
  count           = length(data.aws_availability_zones.available.names)
  depends_on      = [aws_route_table.thispubrtb]
}

# Associate public route tables with DynamoDB VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thispubdynamodb" {
  vpc_endpoint_id = aws_vpc_endpoint.thisdynamoep.id
  route_table_id  = element(aws_route_table.thispubrtb.*.id, count.index)
  count           = length(data.aws_availability_zones.available.names)
  depends_on      = [aws_route_table.thispubrtb]
}