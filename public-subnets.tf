###############################################################################################################
# Public Subnets - Set up Subnets for all availability zones for the PUBLIC zone.
###############################################################################################################

# Create public subnet - 2a
resource "aws_subnet" "thissubpub2a" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.pubsub2a[count.index]
  availability_zone = data.aws_availability_zones.available.names[0]
  count             = length(var.pubsub2a)

  tags = merge(
    local.tags,
    {
      "Name"                   = format("subnet-${var.name}-${var.env}-pub-2a-%02d", count.index + 1)
      "Pub|Prv"                = "public"
      "kubernetes.io/role/elb" = "1"
    },
  )
}

# Create public subnet - 2b
resource "aws_subnet" "thissubpub2b" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.pubsub2b[count.index]
  availability_zone = data.aws_availability_zones.available.names[1]
  count             = length(var.pubsub2b)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-pub-2b-%02d",
        count.index + 11,
      )
      "Pub|Prv"                = "public"
      "kubernetes.io/role/elb" = "1"
    },
  )
}

# Create public subnet - 2c
resource "aws_subnet" "thissubpub2c" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.pubsub2c[count.index]
  availability_zone = data.aws_availability_zones.available.names[2]
  count             = length(var.pubsub2c)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-pub-2c-%02d",
        count.index + 21,
      )
      "Pub|Prv"                = "public"
      "kubernetes.io/role/elb" = "1"
    },
  )
}

# Create public subnet - 2d
resource "aws_subnet" "thissubpub2d" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.pubsub2d[count.index]
  availability_zone = data.aws_availability_zones.available.names[3]
  count             = length(var.pubsub2d)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-pub-2d-%02d",
        count.index + 31,
      )
      "Pub|Prv"                = "public"
      "kubernetes.io/role/elb" = "1"
    },
  )
}