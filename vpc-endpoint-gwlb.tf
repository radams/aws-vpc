# Create VPC endpoint for GWLB Endpoint
resource "aws_vpc_endpoint" "gwlb_2a" {
  count             = var.enableingress ? 1 : 0
  service_name      = var.gwlb_vpc_endpoint_name
  subnet_ids        = aws_subnet.thissubin2a.*.id
  vpc_endpoint_type = "GatewayLoadBalancer"
  vpc_id            = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "vpce-gwlb-ingress-2a-${var.gwlb_vpc_endpoint_name}"
    },
  )
}

resource "aws_vpc_endpoint" "gwlb_2b" {
  count             = var.enableingress ? 1 : 0
  service_name      = var.gwlb_vpc_endpoint_name
  subnet_ids        = aws_subnet.thissubin2b.*.id
  vpc_endpoint_type = "GatewayLoadBalancer"
  vpc_id            = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "vpce-gwlb-ingress-2b-${var.gwlb_vpc_endpoint_name}"
    },
  )
}

resource "aws_vpc_endpoint" "gwlb_2c" {
  count             = var.enableingress ? 1 : 0
  service_name      = var.gwlb_vpc_endpoint_name
  subnet_ids        = aws_subnet.thissubin2c.*.id
  vpc_endpoint_type = "GatewayLoadBalancer"
  vpc_id            = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "vpce-gwlb-ingress-2c-${var.gwlb_vpc_endpoint_name}"
    },
  )
}

# only build in fourth AV if IPs are defined for the ingress subnet
resource "aws_vpc_endpoint" "gwlb_2d" {
  count             = var.enableingress && length(var.insub2d) > 0 ? 1 : 0
  service_name      = var.gwlb_vpc_endpoint_name
  subnet_ids        = aws_subnet.thissubin2d.*.id
  vpc_endpoint_type = "GatewayLoadBalancer"
  vpc_id            = aws_vpc.thisvpc.id

  tags = merge(
    local.tags,
    {
      "Name" = "vpce-gwlb-ingress-2d-${var.gwlb_vpc_endpoint_name}"
    },
  )
}