###############################################################################################################
# Private Subnets - Set up Subnets for all availability zones for the PRIVATE zone.
###############################################################################################################

# Create private subnet - 2a
resource "aws_subnet" "thissubprv2a" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.prvsub2a[count.index]
  availability_zone = data.aws_availability_zones.available.names[0]
  count             = length(var.prvsub2a)

  tags = merge(
    local.tags,
    {
      "Name"                            = format("subnet-${var.name}-${var.env}-prv-2a-%02d", count.index + 1)
      "Pub|Prv"                         = "private"
      "Primary"                         = count.index == 0 ? "true" : "false"
      "kubernetes.io/role/internal-elb" = "1"
    },
  )
}

# Create private subnet - 2b
resource "aws_subnet" "thissubprv2b" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.prvsub2b[count.index]
  availability_zone = data.aws_availability_zones.available.names[1]
  count             = length(var.prvsub2b)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-prv-2b-%02d",
        count.index + 11,
      )
      "Pub|Prv"                         = "private"
      "Primary"                         = count.index == 0 ? "true" : "false"
      "kubernetes.io/role/internal-elb" = "1"
    },
  )
}

# Create private subnet - 2c
resource "aws_subnet" "thissubprv2c" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.prvsub2c[count.index]
  availability_zone = data.aws_availability_zones.available.names[2]
  count             = length(var.prvsub2c)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-prv-2c-%02d",
        count.index + 21,
      )
      "Pub|Prv"                         = "private"
      "Primary"                         = count.index == 0 ? "true" : "false"
      "kubernetes.io/role/internal-elb" = "1"
    },
  )
}

# Create private subnet - 2d
resource "aws_subnet" "thissubprv2d" {
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.prvsub2d[count.index]
  availability_zone = data.aws_availability_zones.available.names[3]
  count             = length(var.prvsub2d)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "subnet-${var.name}-${var.env}-prv-2d-%02d",
        count.index + 31,
      )
      "Pub|Prv"                         = "private"
      "Primary"                         = count.index == 0 ? "true" : "false"
      "kubernetes.io/role/internal-elb" = "1"
    },
  )
}
