###############################################################################################################
# Public Routes - Just for Akamai Site Shield
###############################################################################################################

# Create public routes with Internet Gateway - depends on enable_ingress variable
resource "aws_route" "publicroutes2a_akamai_site_shield" {
  count                  = var.enableingress == false && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[0].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2b_akamai_site_shield" {
  count                  = var.enableingress == false && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[1].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2c_akamai_site_shield" {
  count                  = var.enableingress == false && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[2].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

# all regions have at least 3 AVs, this will prevent trying to make a route in the 4th AV
resource "aws_route" "publicroutes2d_akamai_site_shield" {
  count                  = var.enableingress == false && var.enable_akamai == true && length(var.pubsub2d) > 0 ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[3].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  gateway_id             = aws_internet_gateway.thisigw.id
  depends_on             = [aws_route_table.thispubrtb]
}

# Public Routes - Using Gateway Load Balancer
resource "aws_route" "publicroutes2a_gwlb_akamai_site_shield" {
  count                  = var.enableingress == true && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[0].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2a[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2b_gwlb_akamai_site_shield" {
  count                  = var.enableingress == true && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[1].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2b[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

resource "aws_route" "publicroutes2c_gwlb_akamai_site_shield" {
  count                  = var.enableingress == true && var.enable_akamai == true ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[2].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2c[0].id
  depends_on             = [aws_route_table.thispubrtb]
}

# all regions have at least 3 AVs, this will prevent trying to make a route in the 4th AV
resource "aws_route" "publicroutes2d_gwlb_akamai_site_shield" {
  count                  = var.enableingress == true && var.enable_akamai == true && length(var.pubsub2d) > 0 ? length(local.akamai_site_shield) : 0
  route_table_id         = aws_route_table.thispubrtb[3].id
  destination_cidr_block = local.akamai_site_shield[count.index]
  vpc_endpoint_id        = aws_vpc_endpoint.gwlb_2d[0].id
  depends_on             = [aws_route_table.thispubrtb]
}