###############################################################################################################
# Public NACLs - Set up Network Access Control Lists for the PUBLIC zone.
###############################################################################################################

# Create NACL for public subnets
resource "aws_network_acl" "thisnaclpub" {
  vpc_id = aws_vpc.thisvpc.id

  subnet_ids = concat(
    aws_subnet.thissubpub2a.*.id,
    aws_subnet.thissubpub2b.*.id,
    aws_subnet.thissubpub2c.*.id,
    aws_subnet.thissubpub2d.*.id,
  )

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  tags = merge(
    local.tags,
    {
      "Name" = "acl-${var.name}-${var.env}-pub"
    },
  )
}
