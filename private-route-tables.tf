###############################################################################################################
# Private Route Table - Set up Route Table and Associations for the PRIVATE zone.
###############################################################################################################

# Create private route tables
resource "aws_route_table" "thisprvrtb" {
  vpc_id = aws_vpc.thisvpc.id
  count  = length(data.aws_availability_zones.available.names)

  tags = merge(
    local.tags,
    {
      "Name" = format(
        "rtb-${var.name}-${var.env}-prv-%s",
        element(data.aws_availability_zones.available.names, count.index),
      )
    },
  )
}

# Assign the first private route tables as main route table for the VPC
resource "aws_main_route_table_association" "thismainrtb" {
  vpc_id         = aws_vpc.thisvpc.id
  route_table_id = aws_route_table.thisprvrtb[0].id
}

# Create Private Route to Transit Gateway
resource "aws_route" "thisprvroutedef" {
  #  count                  = length(data.aws_availability_zones.available.names)
  count                  = var.enableprvdefroute == true && var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprvrtb.*.id, count.index)
  destination_cidr_block = var.tgwprefix[0]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprvrtb]
}

resource "aws_route" "thisprvroute1" {
  count                  = var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprvrtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[0]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprvrtb]
}

resource "aws_route" "thisprvroute2" {
  count                  = var.enable_transit_gateway == true ? length(data.aws_availability_zones.available.names) : 0
  route_table_id         = element(aws_route_table.thisprvrtb.*.id, count.index)
  destination_cidr_block = var.prvprefix[1]
  transit_gateway_id     = var.transit_gateway_id
  depends_on             = [aws_route_table.thisprvrtb]
}


# Associate private route tables with private subnets
resource "aws_route_table_association" "thisprvassoc2a" {
  subnet_id      = element(aws_subnet.thissubprv2a.*.id, count.index)
  route_table_id = aws_route_table.thisprvrtb[0].id
  count          = length(var.prvsub2a)
  depends_on     = [aws_route_table.thisprvrtb]
}

resource "aws_route_table_association" "thisprvassoc2b" {
  subnet_id      = element(aws_subnet.thissubprv2b.*.id, count.index)
  route_table_id = aws_route_table.thisprvrtb[1].id
  count          = length(var.prvsub2b)
  depends_on     = [aws_route_table.thisprvrtb]
}

resource "aws_route_table_association" "thisprvassoc2c" {
  subnet_id      = element(aws_subnet.thissubprv2c.*.id, count.index)
  route_table_id = aws_route_table.thisprvrtb[2].id
  count          = length(var.prvsub2c)
  depends_on     = [aws_route_table.thisprvrtb]
}

resource "aws_route_table_association" "thisprvassoc2d" {
  subnet_id      = element(aws_subnet.thissubprv2d.*.id, count.index)
  route_table_id = aws_route_table.thisprvrtb[3].id
  count          = length(var.prvsub2d)
  depends_on     = [aws_route_table.thisprvrtb]
}

# Associate private route tables with S3 VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thisprvs3" {
  vpc_endpoint_id = aws_vpc_endpoint.thiss3ep.id
  route_table_id  = element(aws_route_table.thisprvrtb.*.id, count.index)
  count           = length(data.aws_availability_zones.available.names)
  depends_on      = [aws_route_table.thisprvrtb]
}

# Associate private route tables with DynamoDB VPC Gateway Endpoint
resource "aws_vpc_endpoint_route_table_association" "thisprvdynamodb" {
  vpc_endpoint_id = aws_vpc_endpoint.thisdynamoep.id
  route_table_id  = element(aws_route_table.thisprvrtb.*.id, count.index)
  count           = length(data.aws_availability_zones.available.names)
  depends_on      = [aws_route_table.thisprvrtb]
}
