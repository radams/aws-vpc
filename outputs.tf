# VPC outputs
output "vpcid" {
  description = "ID of the VPC"
  value       = aws_vpc.thisvpc.id
}

output "cidr" {
  description = "CIDR of the VPC"
  value       = var.cidr
}

output "vpcflow_log_group_name" {
  description = "VPC Flowlog Log Group name"
  value       = aws_cloudwatch_log_group.thisloggroup.*.name
}

# Route table outputs
output "prvrtbid" {
  description = "IDs of the Private Route Tables"
  value       = aws_route_table.thisprvrtb.*.id
}

output "pubrtbid" {
  description = "IDs of the Public Route Tables"
  value       = aws_route_table.thispubrtb.*.id
}

output "ctvrtbid" {
  description = "IDs of the Connectivity Route Tables"
  value       = aws_route_table.thisctvrtb.*.id
}

output "inrtbid" {
  description = "IDs of the Ingress Route Tables"
  value       = aws_route_table.thisinrtb.*.id
}

output "igwrtbid" {
  description = "ID of the Internet Gateway Route Table"
  value       = aws_route_table.thisigwrtb.id
}

output "prvrtb_db_id" {
  description = "IDs of the Private Database Route Tables"
  value       = aws_route_table.thisprv_db_rtb.*.id
}

# Private subnets outputs
output "subprv2aid" {
  description = "Private Subnet IDs for 2a"
  value       = aws_subnet.thissubprv2a.*.id
}

output "subprv2bid" {
  description = "Private Subnet IDs for 2b"
  value       = aws_subnet.thissubprv2b.*.id
}

output "subprv2cid" {
  description = "Private Subnet IDs for 2c"
  value       = aws_subnet.thissubprv2c.*.id
}

output "subprv2did" {
  description = "Private Subnet IDs for 2d"
  value       = aws_subnet.thissubprv2d.*.id
}

# Public subnets outputs
output "subpub2aid" {
  description = "Public Subnet IDs for 2a"
  value       = aws_subnet.thissubpub2a.*.id
}

output "subpub2bid" {
  description = "Public Subnet IDs for 2b"
  value       = aws_subnet.thissubpub2b.*.id
}

output "subpub2cid" {
  description = "Public Subnet IDs for 2c"
  value       = aws_subnet.thissubpub2c.*.id
}

output "subpub2did" {
  description = "Public Subnet IDs for 2d"
  value       = aws_subnet.thissubpub2d.*.id
}

# Connectivity subnets outputs
output "subctv2aid" {
  description = "Connectivty Subnet IDs for 2a"
  value       = aws_subnet.thissubctv2a.*.id
}

output "subctv2bid" {
  description = "Connectivty Subnet IDs for 2b"
  value       = aws_subnet.thissubctv2b.*.id
}

output "subctv2cid" {
  description = "Connectivty Subnet IDs for 2c"
  value       = aws_subnet.thissubctv2c.*.id
}

output "subctv2did" {
  description = "Connectivty Subnet IDs for 2d"
  value       = aws_subnet.thissubctv2d.*.id
}

# Ingress subnets outputs
output "subin2aid" {
  description = "Connectivty Subnet IDs for 2a"
  value       = aws_subnet.thissubin2a.*.id
}

output "subin2bid" {
  description = "Connectivty Subnet IDs for 2b"
  value       = aws_subnet.thissubin2b.*.id
}

output "subin2cid" {
  description = "Connectivty Subnet IDs for 2c"
  value       = aws_subnet.thissubin2c.*.id
}

output "subin2did" {
  description = "Connectivty Subnet IDs for 2d"
  value       = aws_subnet.thissubin2d.*.id
}

# Private Database subnets outputs
output "subprv_db_2aid" {
  description = "Private Database Subnet IDs for 2a"
  value       = aws_subnet.thissubprv_db_2a.*.id
}

output "subprv_db_2bid" {
  description = "Private Database Subnet IDs for 2b"
  value       = aws_subnet.thissubprv_db_2b.*.id
}

output "subprv_db_2cid" {
  description = "Private Database Subnet IDs for 2c"
  value       = aws_subnet.thissubprv_db_2c.*.id
}

output "subprv_db_2did" {
  description = "Private Database Subnet IDs for 2d"
  value       = aws_subnet.thissubprv_db_2d.*.id
}

# GWLB VPC endpoints outputs

output "gwlbep2aid" {
  description = "GWLB endpoint ID for 2a"
  value       = aws_vpc_endpoint.gwlb_2a.*.id
}

output "gwlbep2bid" {
  description = "GWLB endpoint ID for 2a"
  value       = aws_vpc_endpoint.gwlb_2b.*.id
}

output "gwlbep2cid" {
  description = "GWLB endpoint ID for 2a"
  value       = aws_vpc_endpoint.gwlb_2c.*.id
}

output "gwlbep2did" {
  description = "GWLB endpoint ID for 2a"
  value       = aws_vpc_endpoint.gwlb_2d.*.id
}

output "security_group_id" {
  description = "Security Group ID, for networking purposes."
  value       = aws_security_group.thissg.id
}