###############################################################################################################
# VPC Endpoints - Set up Gateway Endpoints.
###############################################################################################################

# Create S3 VPC Gateway Endpoint - Routes in xxx-route-tables.tf files.
data "aws_vpc_endpoint_service" "s3" {
  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "thiss3ep" {
  vpc_id       = aws_vpc.thisvpc.id
  service_name = data.aws_vpc_endpoint_service.s3.service_name
  tags         = local.tags
}

# Create DynamoDB VPC Gateway Endpoint - Routes in xxx-route-tables.tf files.
data "aws_vpc_endpoint_service" "dynamodb" {
  service      = "dynamodb"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "thisdynamoep" {
  service_name = data.aws_vpc_endpoint_service.dynamodb.service_name
  vpc_id       = aws_vpc.thisvpc.id
  tags         = local.tags
}

# Create VPC Interface Endpoints and associate to private subnets
resource "aws_vpc_endpoint" "thisintep" {
  vpc_id              = aws_vpc.thisvpc.id
  vpc_endpoint_type   = "Interface"
  service_name        = "com.amazonaws.${var.region}.${var.eplist[count.index]}"
  security_group_ids  = [aws_security_group.thissg.id]
  private_dns_enabled = "1"
  count               = length(var.eplist)
  tags                = local.tags
}

resource "aws_vpc_endpoint_subnet_association" "thisintepassoc2a" {
  subnet_id       = aws_subnet.thissubprv2a[0].id
  vpc_endpoint_id = element(aws_vpc_endpoint.thisintep.*.id, count.index)
  count           = length(var.eplist)
}

resource "aws_vpc_endpoint_subnet_association" "thisintepassoc2b" {
  subnet_id       = aws_subnet.thissubprv2b[0].id
  vpc_endpoint_id = element(aws_vpc_endpoint.thisintep.*.id, count.index)
  count           = length(var.eplist)
}

resource "aws_vpc_endpoint_subnet_association" "thisintepassoc2c" {
  subnet_id       = aws_subnet.thissubprv2c[0].id
  vpc_endpoint_id = element(aws_vpc_endpoint.thisintep.*.id, count.index)
  count           = length(var.eplist)
}

# only build in the fourth AV if IPs are defined for the private subnet
resource "aws_vpc_endpoint_subnet_association" "thisintepassoc2d" {
  subnet_id       = aws_subnet.thissubprv2d[0].id
  vpc_endpoint_id = element(aws_vpc_endpoint.thisintep.*.id, count.index)
  count           = length(var.prvsub2d) > 0 ? length(var.eplist) : 0
}