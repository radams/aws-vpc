###############################################################################################################
# Private Database Subnets - Set up Subnets for all availability zones for the PRIVATE DATABASE zone.
###############################################################################################################

# Create private database subnet - 2a
resource "aws_subnet" "thissubprv_db_2a" {
  count             = var.enable_prv_db_subnets == true ? length(var.dbsub2a) : 0
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.dbsub2a[count.index]
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = merge(
    local.tags,
    {
      "Name"    = format("subnet-${var.name}-${var.env}-prv-db-2a-%02d", count.index + 1)
      "Pub|Prv" = "private-database"
      "Primary" = count.index == 0 ? "true" : "false"
      "db"      = "true"
    },
  )
}

# Create private database subnet - 2b
resource "aws_subnet" "thissubprv_db_2b" {
  count             = var.enable_prv_db_subnets == true ? length(var.dbsub2b) : 0
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.dbsub2b[count.index]
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = merge(
    local.tags,
    {
      "Name"    = format("subnet-${var.name}-${var.env}-prv-db-2b-%02d", count.index + 11)
      "Pub|Prv" = "private-database"
      "Primary" = count.index == 0 ? "true" : "false"
      "db"      = "true"
    },
  )
}

# Create private database subnet - 2c
resource "aws_subnet" "thissubprv_db_2c" {
  count             = var.enable_prv_db_subnets == true ? length(var.dbsub2c) : 0
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.dbsub2c[count.index]
  availability_zone = data.aws_availability_zones.available.names[2]

  tags = merge(
    local.tags,
    {
      "Name"    = format("subnet-${var.name}-${var.env}-prv-db-2c-%02d", count.index + 21)
      "Pub|Prv" = "private-database"
      "Primary" = count.index == 0 ? "true" : "false"
      "db"      = "true"
    },
  )
}

# Create private database subnet - 2d
resource "aws_subnet" "thissubprv_db_2d" {
  count             = var.enable_prv_db_subnets == true ? length(var.dbsub2d) : 0
  vpc_id            = aws_vpc.thisvpc.id
  cidr_block        = var.dbsub2d[count.index]
  availability_zone = data.aws_availability_zones.available.names[3]

  tags = merge(
    local.tags,
    {
      "Name"    = format("subnet-${var.name}-${var.env}-prv-db-2d-%02d", count.index + 31)
      "Pub|Prv" = "private-database"
      "Primary" = count.index == 0 ? "true" : "false"
      "db"      = "true"
    },
  )
}