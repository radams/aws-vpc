variable "name" {
  description = "Business unit name: 5 or 6 letter code (ex: merch)."
}

variable "region" {
  description = "The AWS region. ex. us-west-2"
}

variable "env" {
  description = "The environment type, ex. dev or prod."
}

variable "tags" {
  description = "Map of tags."
  type        = map(string)
}

variable "defprefix" {
  description = "Default gateway prefix."
  default     = "0.0.0.0/0"
}

variable "prvprefix" {
  description = "List of private prefixes for routing"
  type        = list(string)
  default     = ["10.0.0.0/8", "172.16.0.0/12"]
}

variable "tgwprefix" {
  description = "List of prefixes for Transit Gateway (TGW) routes."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "sgports" {
  description = "TCP ports used for security groups"
  type        = list(string)
  default     = ["80", "443"]
}

variable "cidr" {
  description = "The CIDR of the VPC"
}

variable "prvsub2a" {
  description = "List of private subnets in AZ 2a"
  type        = list(string)
}

variable "prvsub2b" {
  description = "List of private subnets in AZ 2b"
  type        = list(string)
}

variable "prvsub2c" {
  description = "List of private subnets in AZ 2c"
  type        = list(string)
}

variable "prvsub2d" {
  description = "List of private subnets in AZ 2d"
  type        = list(string)
}

variable "pubsub2a" {
  description = "List of public subnets in AZ 2a"
  type        = list(string)
}

variable "pubsub2b" {
  description = "List of public subnets in AZ 2b"
  type        = list(string)
}

variable "pubsub2c" {
  description = "List of public subnets in AZ 2c"
  type        = list(string)
}

variable "pubsub2d" {
  description = "List of public subnets in AZ 2d"
  type        = list(string)
}

variable "ctvsub2a" {
  description = "List of connectivity subnets in AZ 2a"
  type        = list(string)
}

variable "ctvsub2b" {
  description = "List of connectivity subnets in AZ 2b"
  type        = list(string)
}

variable "ctvsub2c" {
  description = "List of connectivity subnets in AZ 2c"
  type        = list(string)
}

variable "ctvsub2d" {
  description = "List of connectivity subnets in AZ 2d"
  type        = list(string)
}

variable "insub2a" {
  description = "List of ingress subnets in AZ 2a"
  type        = list(string)
}

variable "insub2b" {
  description = "List of ingress subnets in AZ 2b"
  type        = list(string)
}

variable "insub2c" {
  description = "List of ingress subnets in AZ 2c"
  type        = list(string)
}

variable "insub2d" {
  description = "List of ingress subnets in AZ 2d"
  type        = list(string)
}

# db subnets 
variable "enable_prv_db_subnets" {
  description = "Flag to enable the database subnets"
  type        = bool
  default     = false
}

variable "dbsub2a" {
  description = "List of private database subnets in AZ 2a"
  type        = list(string)
  default     = []
}

variable "dbsub2b" {
  description = "List of private database subnets in AZ 2b"
  type        = list(string)
  default     = []
}

variable "dbsub2c" {
  description = "List of private database subnets in AZ 2c"
  type        = list(string)
  default     = []
}

variable "dbsub2d" {
  description = "List of private database subnets in AZ 2d"
  type        = list(string)
  default     = []
}

variable "reidomain" {
  description = "REI domain for DHCP option sets"
  default     = "reicorpnet"
}

variable "reidnsservers" {
  description = "REI DNS servers for DHCP option sets"
  type        = list(string)
  default     = ["10.7.24.12", "10.7.24.13", "10.9.24.12", "10.9.24.13"]
}

variable "enableflowlog" {
  description = "Indicate whether to enable VPC Flowlog"
}

variable "enableinternet" {
  description = "Indicate whether to add default route to public subnets via Internet Gateway"
  type        = bool
  default     = false
}

variable "enableingress" {
  description = "Flag to enable ingress routes through the PaloAlto and GWLB."
  type        = bool
  default     = false
}

variable "enableprvdefroute" {
  description = "Indicate whether to disable the default route in private subnets to the TGW"
  type        = bool
  default     = true
}

variable "gwlb_vpc_endpoint_name" {
  type        = string
  description = "The AWS service name"
  default     = "com.amazonaws.vpce.us-west-2.vpce-svc-03923d52e0f31c510"
}

variable "enable_tenable" {
  description = "Flag to enable the Tenable Cloud Scanners routes in the public route tables"
  type        = bool
  default     = false
}

variable "enable_akamai" {
  description = "Flag to enable the Akamai routes in the public route tables"
  type        = bool
  default     = true
}

variable "enable_open_ingress" {
  description = "Flag to enable a 0.0.0.0/0 route in the public route tables"
  type        = bool
  default     = false
}

variable "eplist" {
  description = "The list of Endpoint Interfaces that will be created"
  type        = list(string)
}

variable "transit_gateway_id" {
  description = "Transit Gateway ID"
  type        = string
  default     = ""
}

variable "enable_transit_gateway" {
  description = "Enable Transit Gateway Routes"
  type        = bool
  default     = true
}