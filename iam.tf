# Role policy document for the VPC flow log resource

data "aws_iam_policy_document" "flowlogrole" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }
  }
}

# Create the IAM policy for the VPC flow log resource

resource "aws_iam_policy" "thisflowlogpolicy" {
  name   = "flow-log-policy"
  policy = data.aws_iam_policy_document.flowlogpolicy.json
  count  = var.enableflowlog ? 1 : 0
}

# Policy policy document for the VPC flow log resource

data "aws_iam_policy_document" "flowlogpolicy" {
  statement {
    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
    ]

    effect = "Allow"

    resources = [
      "*",
    ]
  }
}

# Create the IAM role for the VPC flow log resource
resource "aws_iam_role" "thisflowlogrole" {
  name               = "cld-global-flowlog"
  assume_role_policy = data.aws_iam_policy_document.flowlogrole.json
  count              = var.enableflowlog ? 1 : 0
  tags               = local.tags
}

# Attach the flow log policy to the flow log role
resource "aws_iam_role_policy_attachment" "thisflowlogattach" {
  role       = aws_iam_role.thisflowlogrole[count.index].name
  policy_arn = aws_iam_policy.thisflowlogpolicy[count.index].arn
  count      = var.enableflowlog ? 1 : 0
}
