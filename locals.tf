locals {
  # add more tags! 
  tags = merge(
    var.tags,
    {
      "terraform"       = "true"
      "tfmodule"        = "https://git.rei.com/projects/TFMODS/repos/aws-vpc"
      "tfmoduleVersion" = "12.28"
    },
  )

  # Akamai Site Shield Updated 4/28
  akamai_site_shield = [
    "104.117.66.0/24",
    "104.119.189.0/24",
    "104.70.164.0/24",
    "104.93.21.0/24",
    "165.254.96.0/24",
    "168.143.242.0/24",
    "168.143.243.0/24",
    "184.28.194.0/24",
    "184.51.101.0/24",
    "204.2.243.0/24",
    "23.201.57.0/24",
    "23.35.71.0/24",
    "23.47.58.0/24",
    "23.48.209.0/24",
    "23.48.210.0/24",
    "23.48.94.0/24",
    "23.52.41.0/24",
    "23.52.43.0/24",
    "23.56.169.0/24",
    "23.59.176.0/24",
    "23.77.218.0/24",
    "66.198.8.0/24",
    "67.220.142.0/24",
    "69.174.30.128/25",
    "96.7.74.0/24"
  ]

  # Akamai Staging 
  akamai_staging = [
    "67.220.142.22/32",
    "67.220.142.21/32",
    "67.220.142.20/32",
    "67.220.142.19/32",
    "66.198.8.141/32",
    "66.198.8.142/32",
    "66.198.8.143/32",
    "66.198.8.144/32",
    "23.48.168.0/22",
    "23.50.48.0/20"
  ]

  # Public routes
  base_public_routes = [
    # Apigee Edge NAT (Southbound)
    "34.82.106.153/32",
    "35.233.205.119/32",
    # Apigee TIP (Southbound)
    "35.227.131.200/30",
    # AWS DX public vif
    "54.239.255.168/30",
    # AWS Internet egress
    "34.223.246.90/32",
    "100.20.136.107/32",
    "52.13.159.210/32",
    "100.21.115.82/32",
    # REI-owned public IPs
    "204.89.16.0/22",
    # Switch-owned public IPs
    "64.79.147.0/25",
    # Issaquah IPs
    "50.230.235.8/30",
    # Sumner IPs
    "50.231.103.216/29",
    "65.140.69.112/29"
  ]

  # tenable sensor ips
  tenable_ips = [
    "13.59.252.0/25",
    "54.175.125.192/26",
    "34.201.223.128/25",
    "3.132.217.0/25",
    "18.116.198.0/24",
    "44.192.244.0/24",
    "54.219.188.128/26",
    "13.56.21.128/25",
    "34.223.64.0/25",
    "44.242.181.128/25",
    "3.101.175.0/25",
    "35.82.51.128/25",
    "35.86.126.0/24"
  ]

  # concat the public routes with the tenable routes
  base_public_routes_with_tenable = concat(local.base_public_routes, local.tenable_ips)

  # if tenable is enabled use the public_routes_concat value, else the normal publicroutes value
  public_routes_list = var.enable_tenable ? local.base_public_routes_with_tenable : local.base_public_routes

  # if we need a full open ingress then enable the enable_open_ingress flag to have a public_route of 0.0.0.0/0
  public_routes = var.enable_open_ingress == true && var.enableingress == true ? ["0.0.0.0/0"] : local.public_routes_list
}