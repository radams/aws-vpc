###############################################################################################################
# Private Datbase NACLs - Set up Network Access Control Lists for the PRIVATE DATABASE zone.
###############################################################################################################

# Create NACL for private database subnets
resource "aws_network_acl" "thisnaclprv_db" {
  count = var.enable_prv_db_subnets == true ? 1 : 0

  vpc_id = aws_vpc.thisvpc.id

  subnet_ids = concat(
    aws_subnet.thissubprv_db_2a.*.id,
    aws_subnet.thissubprv_db_2b.*.id,
    aws_subnet.thissubprv_db_2c.*.id,
    aws_subnet.thissubprv_db_2d.*.id,
  )

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  tags = merge(
    local.tags,
    {
      "Name" = "acl-${var.name}-${var.env}-prv-db"
      "db"   = "true"
    },
  )
}
