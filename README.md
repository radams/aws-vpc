# AWS VPC Terraform Module

This module creates an AWS VPC suitable for REI's network.

## Features
- [x] Creates VPC with /18 CIDR. The second octet will be unique per line of business
- [x] Creates private and public subnets within VPC. Quantity and CIDR is set in the .tfvars file
- [x] Creates private and public route tables, one each per AZ
- [x] Creates connectivity subnets and route tables, one each per AZ, for use with Transit Gateway
- [x] Creates a Network ACL (open to everywhere) and attaches to all subnets
- [x] Creates a generic Security Group that allows ingress from anywhere and egress to 10.0.0.0/8 and 172.16.0.0/12
- [x] Creates an Internet Gateway and attaches to the VPC
- [x] Creates a Virtual Gateway (Amazon-side ASN defined in .tfvars) and attaches to VPC
- [x] Creates S3 and DynamoDB VPC Gateway Endpoints and attaches to all route tables
- [x] Creates a DHCP Option Set and associates to the VPC
- [x] Creates a Customer Gateway for the Kent VPN appliance
- [x] Creates a VPN Connection to the Kent VPN appliance

## Usage

In the **aws-account-xxxxx** repository for the account to be deployed, modify the *.tfvars* file located in the *configuration/* directory to set the values for the Inputs listed below

Please look at [./example](example) for some sample Terraform.

---
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.12 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.thisloggroup](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_flow_log.thisflowlog](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/flow_log) | resource |
| [aws_iam_policy.thisflowlogpolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.thisflowlogrole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.thisflowlogattach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_internet_gateway.thisigw](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway) | resource |
| [aws_main_route_table_association.thismainrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/main_route_table_association) | resource |
| [aws_network_acl.thisnaclctv](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl.thisnaclin](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl.thisnaclprv](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl.thisnaclprv_db](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_network_acl.thisnaclpub](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/network_acl) | resource |
| [aws_route.igwroute2a_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.igwroute2b_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.igwroute2c_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.igwroute2d_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a_gwlb_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2a_gwlb_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b_gwlb_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2b_gwlb_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c_gwlb_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2c_gwlb_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d_gwlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d_gwlb_akamai_site_shield](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.publicroutes2d_gwlb_akamai_staging](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.pubrtb_thisprvroute_0](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.pubrtb_thisprvroute_1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisinroute](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprv_db_route1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprv_db_route2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprv_db_routedef](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprvroute1](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprvroute2](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thisprvroutedef](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route.thispubroute](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route) | resource |
| [aws_route_table.thisctvrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.thisigwrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.thisinrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.thisprv_db_rtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.thisprvrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table.thispubrtb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table) | resource |
| [aws_route_table_association.thisctvassoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisctvassoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisctvassoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisctvassoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisigwassoc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisinassoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisinassoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisinassoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisinassoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprv_db_assoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprv_db_assoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprv_db_assoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprv_db_assoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprvassoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprvassoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprvassoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thisprvassoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thispubassoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thispubassoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thispubassoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_route_table_association.thispubassoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table_association) | resource |
| [aws_security_group.thissg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_servicequotas_service_quota.routesperroutetable](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/servicequotas_service_quota) | resource |
| [aws_subnet.thissubctv2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubctv2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubctv2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubctv2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubin2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubin2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubin2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubin2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv_db_2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv_db_2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv_db_2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubprv_db_2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubpub2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubpub2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubpub2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_subnet.thissubpub2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/subnet) | resource |
| [aws_vpc.thisvpc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc) | resource |
| [aws_vpc_dhcp_options.newdopt](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_dhcp_options) | resource |
| [aws_vpc_dhcp_options_association.newdoptassoc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_dhcp_options_association) | resource |
| [aws_vpc_endpoint.gwlb_2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.gwlb_2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.gwlb_2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.gwlb_2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.thisdynamoep](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.thisintep](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint.thiss3ep](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint) | resource |
| [aws_vpc_endpoint_route_table_association.thisprv_db_dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.thisprv_db_s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.thisprvdynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.thisprvs3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.thispubdynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_route_table_association.thispubs3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_route_table_association) | resource |
| [aws_vpc_endpoint_subnet_association.thisintepassoc2a](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_subnet_association) | resource |
| [aws_vpc_endpoint_subnet_association.thisintepassoc2b](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_subnet_association) | resource |
| [aws_vpc_endpoint_subnet_association.thisintepassoc2c](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_subnet_association) | resource |
| [aws_vpc_endpoint_subnet_association.thisintepassoc2d](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc_endpoint_subnet_association) | resource |
| [aws_availability_zones.available](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) | data source |
| [aws_iam_policy_document.flowlogpolicy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.flowlogrole](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_vpc_endpoint_service.dynamodb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc_endpoint_service) | data source |
| [aws_vpc_endpoint_service.s3](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc_endpoint_service) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cidr"></a> [cidr](#input\_cidr) | The CIDR of the VPC | `any` | n/a | yes |
| <a name="input_ctvsub2a"></a> [ctvsub2a](#input\_ctvsub2a) | List of connectivity subnets in AZ 2a | `list(string)` | n/a | yes |
| <a name="input_ctvsub2b"></a> [ctvsub2b](#input\_ctvsub2b) | List of connectivity subnets in AZ 2b | `list(string)` | n/a | yes |
| <a name="input_ctvsub2c"></a> [ctvsub2c](#input\_ctvsub2c) | List of connectivity subnets in AZ 2c | `list(string)` | n/a | yes |
| <a name="input_ctvsub2d"></a> [ctvsub2d](#input\_ctvsub2d) | List of connectivity subnets in AZ 2d | `list(string)` | n/a | yes |
| <a name="input_dbsub2a"></a> [dbsub2a](#input\_dbsub2a) | List of private database subnets in AZ 2a | `list(string)` | `[]` | no |
| <a name="input_dbsub2b"></a> [dbsub2b](#input\_dbsub2b) | List of private database subnets in AZ 2b | `list(string)` | `[]` | no |
| <a name="input_dbsub2c"></a> [dbsub2c](#input\_dbsub2c) | List of private database subnets in AZ 2c | `list(string)` | `[]` | no |
| <a name="input_dbsub2d"></a> [dbsub2d](#input\_dbsub2d) | List of private database subnets in AZ 2d | `list(string)` | `[]` | no |
| <a name="input_defprefix"></a> [defprefix](#input\_defprefix) | Default gateway prefix. | `string` | `"0.0.0.0/0"` | no |
| <a name="input_enable_akamai"></a> [enable\_akamai](#input\_enable\_akamai) | Flag to enable the Akamai routes in the public route tables | `bool` | `true` | no |
| <a name="input_enable_open_ingress"></a> [enable\_open\_ingress](#input\_enable\_open\_ingress) | Flag to enable a 0.0.0.0/0 route in the public route tables | `bool` | `false` | no |
| <a name="input_enable_prv_db_subnets"></a> [enable\_prv\_db\_subnets](#input\_enable\_prv\_db\_subnets) | Flag to enable the database subnets | `bool` | `false` | no |
| <a name="input_enable_tenable"></a> [enable\_tenable](#input\_enable\_tenable) | Flag to enable the Tenable Cloud Scanners routes in the public route tables | `bool` | `false` | no |
| <a name="input_enable_transit_gateway"></a> [enable\_transit\_gateway](#input\_enable\_transit\_gateway) | Enable Transit Gateway Routes | `bool` | `true` | no |
| <a name="input_enableflowlog"></a> [enableflowlog](#input\_enableflowlog) | Indicate whether to enable VPC Flowlog | `any` | n/a | yes |
| <a name="input_enableingress"></a> [enableingress](#input\_enableingress) | Flag to enable ingress routes through the PaloAlto and GWLB. | `bool` | `false` | no |
| <a name="input_enableinternet"></a> [enableinternet](#input\_enableinternet) | Indicate whether to add default route to public subnets via Internet Gateway | `bool` | `false` | no |
| <a name="input_enableprvdefroute"></a> [enableprvdefroute](#input\_enableprvdefroute) | Indicate whether to disable the default route in private subnets to the TGW | `bool` | `true` | no |
| <a name="input_env"></a> [env](#input\_env) | The environment type, ex. dev or prod. | `any` | n/a | yes |
| <a name="input_eplist"></a> [eplist](#input\_eplist) | The list of Endpoint Interfaces that will be created | `list(string)` | n/a | yes |
| <a name="input_gwlb_vpc_endpoint_name"></a> [gwlb\_vpc\_endpoint\_name](#input\_gwlb\_vpc\_endpoint\_name) | The AWS service name | `string` | `"com.amazonaws.vpce.us-west-2.vpce-svc-03923d52e0f31c510"` | no |
| <a name="input_insub2a"></a> [insub2a](#input\_insub2a) | List of ingress subnets in AZ 2a | `list(string)` | n/a | yes |
| <a name="input_insub2b"></a> [insub2b](#input\_insub2b) | List of ingress subnets in AZ 2b | `list(string)` | n/a | yes |
| <a name="input_insub2c"></a> [insub2c](#input\_insub2c) | List of ingress subnets in AZ 2c | `list(string)` | n/a | yes |
| <a name="input_insub2d"></a> [insub2d](#input\_insub2d) | List of ingress subnets in AZ 2d | `list(string)` | n/a | yes |
| <a name="input_name"></a> [name](#input\_name) | Business unit name: 5 or 6 letter code (ex: merch). | `any` | n/a | yes |
| <a name="input_prvprefix"></a> [prvprefix](#input\_prvprefix) | List of private prefixes for routing | `list(string)` | <pre>[<br>  "10.0.0.0/8",<br>  "172.16.0.0/12"<br>]</pre> | no |
| <a name="input_prvsub2a"></a> [prvsub2a](#input\_prvsub2a) | List of private subnets in AZ 2a | `list(string)` | n/a | yes |
| <a name="input_prvsub2b"></a> [prvsub2b](#input\_prvsub2b) | List of private subnets in AZ 2b | `list(string)` | n/a | yes |
| <a name="input_prvsub2c"></a> [prvsub2c](#input\_prvsub2c) | List of private subnets in AZ 2c | `list(string)` | n/a | yes |
| <a name="input_prvsub2d"></a> [prvsub2d](#input\_prvsub2d) | List of private subnets in AZ 2d | `list(string)` | n/a | yes |
| <a name="input_pubsub2a"></a> [pubsub2a](#input\_pubsub2a) | List of public subnets in AZ 2a | `list(string)` | n/a | yes |
| <a name="input_pubsub2b"></a> [pubsub2b](#input\_pubsub2b) | List of public subnets in AZ 2b | `list(string)` | n/a | yes |
| <a name="input_pubsub2c"></a> [pubsub2c](#input\_pubsub2c) | List of public subnets in AZ 2c | `list(string)` | n/a | yes |
| <a name="input_pubsub2d"></a> [pubsub2d](#input\_pubsub2d) | List of public subnets in AZ 2d | `list(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | The AWS region. ex. us-west-2 | `any` | n/a | yes |
| <a name="input_reidnsservers"></a> [reidnsservers](#input\_reidnsservers) | REI DNS servers for DHCP option sets | `list(string)` | <pre>[<br>  "10.7.24.12",<br>  "10.7.24.13",<br>  "10.9.24.12",<br>  "10.9.24.13"<br>]</pre> | no |
| <a name="input_reidomain"></a> [reidomain](#input\_reidomain) | REI domain for DHCP option sets | `string` | `"reicorpnet"` | no |
| <a name="input_sgports"></a> [sgports](#input\_sgports) | TCP ports used for security groups | `list(string)` | <pre>[<br>  "80",<br>  "443"<br>]</pre> | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Map of tags. | `map(string)` | n/a | yes |
| <a name="input_tgwprefix"></a> [tgwprefix](#input\_tgwprefix) | List of prefixes for Transit Gateway (TGW) routes. | `list(string)` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_transit_gateway_id"></a> [transit\_gateway\_id](#input\_transit\_gateway\_id) | Transit Gateway ID | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cidr"></a> [cidr](#output\_cidr) | CIDR of the VPC |
| <a name="output_ctvrtbid"></a> [ctvrtbid](#output\_ctvrtbid) | IDs of the Connectivity Route Tables |
| <a name="output_gwlbep2aid"></a> [gwlbep2aid](#output\_gwlbep2aid) | GWLB endpoint ID for 2a |
| <a name="output_gwlbep2bid"></a> [gwlbep2bid](#output\_gwlbep2bid) | GWLB endpoint ID for 2a |
| <a name="output_gwlbep2cid"></a> [gwlbep2cid](#output\_gwlbep2cid) | GWLB endpoint ID for 2a |
| <a name="output_gwlbep2did"></a> [gwlbep2did](#output\_gwlbep2did) | GWLB endpoint ID for 2a |
| <a name="output_igwrtbid"></a> [igwrtbid](#output\_igwrtbid) | ID of the Internet Gateway Route Table |
| <a name="output_inrtbid"></a> [inrtbid](#output\_inrtbid) | IDs of the Ingress Route Tables |
| <a name="output_prvrtb_db_id"></a> [prvrtb\_db\_id](#output\_prvrtb\_db\_id) | IDs of the Private Database Route Tables |
| <a name="output_prvrtbid"></a> [prvrtbid](#output\_prvrtbid) | IDs of the Private Route Tables |
| <a name="output_pubrtbid"></a> [pubrtbid](#output\_pubrtbid) | IDs of the Public Route Tables |
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | Security Group ID, for networking purposes. |
| <a name="output_subctv2aid"></a> [subctv2aid](#output\_subctv2aid) | Connectivty Subnet IDs for 2a |
| <a name="output_subctv2bid"></a> [subctv2bid](#output\_subctv2bid) | Connectivty Subnet IDs for 2b |
| <a name="output_subctv2cid"></a> [subctv2cid](#output\_subctv2cid) | Connectivty Subnet IDs for 2c |
| <a name="output_subctv2did"></a> [subctv2did](#output\_subctv2did) | Connectivty Subnet IDs for 2d |
| <a name="output_subin2aid"></a> [subin2aid](#output\_subin2aid) | Connectivty Subnet IDs for 2a |
| <a name="output_subin2bid"></a> [subin2bid](#output\_subin2bid) | Connectivty Subnet IDs for 2b |
| <a name="output_subin2cid"></a> [subin2cid](#output\_subin2cid) | Connectivty Subnet IDs for 2c |
| <a name="output_subin2did"></a> [subin2did](#output\_subin2did) | Connectivty Subnet IDs for 2d |
| <a name="output_subprv2aid"></a> [subprv2aid](#output\_subprv2aid) | Private Subnet IDs for 2a |
| <a name="output_subprv2bid"></a> [subprv2bid](#output\_subprv2bid) | Private Subnet IDs for 2b |
| <a name="output_subprv2cid"></a> [subprv2cid](#output\_subprv2cid) | Private Subnet IDs for 2c |
| <a name="output_subprv2did"></a> [subprv2did](#output\_subprv2did) | Private Subnet IDs for 2d |
| <a name="output_subprv_db_2aid"></a> [subprv\_db\_2aid](#output\_subprv\_db\_2aid) | Private Database Subnet IDs for 2a |
| <a name="output_subprv_db_2bid"></a> [subprv\_db\_2bid](#output\_subprv\_db\_2bid) | Private Database Subnet IDs for 2b |
| <a name="output_subprv_db_2cid"></a> [subprv\_db\_2cid](#output\_subprv\_db\_2cid) | Private Database Subnet IDs for 2c |
| <a name="output_subprv_db_2did"></a> [subprv\_db\_2did](#output\_subprv\_db\_2did) | Private Database Subnet IDs for 2d |
| <a name="output_subpub2aid"></a> [subpub2aid](#output\_subpub2aid) | Public Subnet IDs for 2a |
| <a name="output_subpub2bid"></a> [subpub2bid](#output\_subpub2bid) | Public Subnet IDs for 2b |
| <a name="output_subpub2cid"></a> [subpub2cid](#output\_subpub2cid) | Public Subnet IDs for 2c |
| <a name="output_subpub2did"></a> [subpub2did](#output\_subpub2did) | Public Subnet IDs for 2d |
| <a name="output_vpcflow_log_group_name"></a> [vpcflow\_log\_group\_name](#output\_vpcflow\_log\_group\_name) | VPC Flowlog Log Group name |
| <a name="output_vpcid"></a> [vpcid](#output\_vpcid) | ID of the VPC |
<!-- END_TF_DOCS -->
---

### Version History
**1.0** - Creates VPC with one NAT Gateway in each AZ and points default route of private route table to the NAT Gateways
Public route tables have default route pointing to the Internet Gateway

**1.3** - Does not create NAT Gateways. Default route for private route table points to VGW

**1.5** - No NAT gateways, no static routes created with the route tables, add VGW ID output

**1.6** - Change *octet* variable to *cidr* for multi-region readiness

**1.7** - Add variable to enable/disable VPN, include outputs for VPC peering, remove CloudWatch alarms

**1.8** - Update to subnet tags to include "Pub|Prv" for use by DigitalRetail for Apline stack.

**1.9** - Add DHCP Option Set for REI DNS resolution and include tag to include which subnet is Primary

**1.10** - Update to subnet tags to include "Primary" for use by DigitalRetail for Apline stack to help select the primary subnet.

**1.11** - Add DynamoDB endpoint and associate with public and private route tables

**1.12** - Add VPC flow logs

**1.13** - Add interface endpoints

**1.14** - Add connectivity subnets for use with TGW

**1.15** - Add support for a fourth availability zone.  Add Subnet Outputs.

**1.16** - Add Outputs for the Subnet IDs.

**1.17** - Separate variables into new file; add static routes for TGW

**1.18** - Disable DHCP Options Sets for the VPC

**1.19** - Remove VGW, change static routes to TGW, remove route table propagation, remove VPN and customer gateway

**1.19.b** - Update private route tables to have the Transit Gateway route as a seperate resource.  Output Cloudwatch Log group for VPCFlowLogs.

**1.20** - Add tags for all resources that accept them.  Code re-organization.

**1.20i** - Add a default route to public subnets to the Internet Gateway

**12.0** - Terraform 12 upgrade

**12.1** - Remove the lifecycle/ignore_changes/tags block on the VPC and Private Subnets.  To be replaced with options in the AWS provider. - RTA

**12.2** - Add Akamai Site Shield as routes to point to Internet Gateway in public route tables - PC

**12.3** - Add REI public IPs as routes to point to Internet Gateway in public route tables - PC

**12.4** - Add Apigee Edge NAT IPs as routes to point to Internet Gateway in public route tables - PC

**12.5** - Remove extraneous variables and commented out legacy configuration - PC

**12.6** - Update Akamai Site Shield prefixes - PC

**12.7** - Update Akamai Site Shield prefixes, add Sumner IP ranges and AWS Internet egress IPs to public route tables - PC

**12.8** - Add Akamai staging prefixes. Update `aws_vpc_endpoint_service` to specify gateway. - RA

**12.9** - Update Akamai Site Shield prefixes - PC

**12.10** - Update Akamai Site Shield prefixes - PC

**12.11** - Add EKS Load Balancer tagging to private subnets - RA

**12.12** - Add EKS Load Balancer tagging to public subnets, add ingress subnets, add IGW route table, update Akamai Site Shield prefixes - PC

**12.13** - Add DHCP options set for domain names to be `rei.com` and `reicorpnet.com`

**12.14** - Update Akamai Site Shield prefixes, add `DO NOT USE` prefix to connectivity and ingress subnets

**12.15** - Update Akamai Site Shield prefixes

**12.16** - Add Apigee TIP prefixes, Issaquah office prefix, all AWS Internet egress IPs

**12.17** - Update Akamai Staging IPs. Move GatewayLoadBalancer VPCEndpoints to this module.

**12.18** - Update Akamai IPs, add outputs for GWLB endpoints, add "enable" variable for default route in private route tables

**12.19** - Add flag for Tenable ingress, this will concat the publicroute ips list with the tenable ip list.

**12.20** - Remove obsolete Akamai staging IPs, update Site Shield for March update, move `publicroutes` to local variable

**12.21** - Add flag to enable private database subnets, add resources for the private database subnets, route tables, nacls, etc.

**12.22** - Seperate the Akamai IPs to their own lists/resources to help reduce the number of route changes when they update.  Also, update the Akamai Site Shield IPs.

**12.23** - Remove remote state to get Transit Gateway ID, pass in as a variable instead. Add in logic to not use the fourth Availability Zone if not needed.

**12.24** - Add `enable_transit_gateway` variable to toggle on the TGW.

**12.25** - Fix DCHP to be region specific.

**12.26** - January 2023 Akamai route updates

**12.27** - Add security group output, make public routes a bit more clear

**12.27** - Add security group output, make public routes a bit more clear

**12.28** - Add `enable_akamai_routes` variable to control akamai routes.  Add `enable_open_ingress` to force total open ingress.


### Authors
Paul Casey - pcasey@rei.com
Rick Adams - radams@rei.com
