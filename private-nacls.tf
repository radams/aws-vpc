###############################################################################################################
# Private NACLs - Set up Network Access Control Lists for the PRIVATE zone.
###############################################################################################################

# Create NACL for private subnets
resource "aws_network_acl" "thisnaclprv" {
  vpc_id = aws_vpc.thisvpc.id

  subnet_ids = concat(
    aws_subnet.thissubprv2a.*.id,
    aws_subnet.thissubprv2b.*.id,
    aws_subnet.thissubprv2c.*.id,
    aws_subnet.thissubprv2d.*.id,
  )

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.defprefix
    from_port  = 0
    to_port    = 0
  }

  tags = merge(
    local.tags,
    {
      "Name" = "acl-${var.name}-${var.env}-prv"
    },
  )
}
